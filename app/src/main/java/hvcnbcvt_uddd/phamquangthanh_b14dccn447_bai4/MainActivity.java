package hvcnbcvt_uddd.phamquangthanh_b14dccn447_bai4;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private MySharedPreferences mySharedPrecence;
    Button btnAnh, btnVietNam;
    int kt = 0;


        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
            mySharedPrecence = new MySharedPreferences(this);
            Intent i = getIntent();
        if (mySharedPrecence.getData(MySharedPreferences.NGON_NGU) != null && i.getStringExtra("kt") == null)
        {
            ganNgonNgu(mySharedPrecence.getData(MySharedPreferences.NGON_NGU));
        }

        btnAnh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mySharedPrecence.setData(MySharedPreferences.NGON_NGU,"en");
                ganNgonNgu("en");

            }
        });

        btnVietNam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mySharedPrecence.setData(MySharedPreferences.NGON_NGU,"vi");
                ganNgonNgu("vi");
            }
        });
    }
    public void ganNgonNgu(String language) {
        Locale locale = new Locale(language);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(
                config, getBaseContext().getResources().getDisplayMetrics()
        );
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.putExtra("kt", "1");
        startActivity(intent);
    }

    private void initView() {
        btnAnh = (Button) findViewById(R.id.btnAnh);
        btnVietNam  = (Button) findViewById(R.id.btnVietNam);
    }
}
