package hvcnbcvt_uddd.phamquangthanh_b14dccn447_bai4;

/**
 * Created by thanh on 26/03/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;


    public class MySharedPreferences implements SharedPreferences.OnSharedPreferenceChangeListener {
        public static final String NGON_NGU="ngon_ngu";

        private SharedPreferences sharedPreferences;
        public MySharedPreferences(Context context){// tao sharedpre
            sharedPreferences=context.getSharedPreferences("MySharedPreference",Context.MODE_PRIVATE);
        }
        public void setData (String key,String value){ // set vao thi can key va value
            SharedPreferences.Editor editor= sharedPreferences.edit();
            editor.putString(key,value);
            editor.commit();
        }
        public String  getData(String key){ // lay ra thi chi can key
            String value=sharedPreferences.getString(key,null);
            return value;
        }
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // lang nghe su kien khi du dieu tren no bi thay doi

        }
        public void registerListenDataChange(){
            sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        }
        public void unRegisterListenDataChange(){
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        }
    }

